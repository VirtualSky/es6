export function parseUrl() {
    const url = window.location;
    const query = url.href.split('?')[1] || '';
    const delimiter = '&';
    return query.split(delimiter)
        .map(el => el.split("="))
        .reduce((acc, el) => { acc[el[0]] = el[1]; return acc }, {});
}
