// TODO Step 6 import "./score.component.html"
import { parseUrl } from '../../utils/utils';
(function () {      // TODO Step 6 remove this closure

    /* class ScoreComponent constructor */
    class ScoreComponent {
        constructor() {
            var params = parseUrl();
            this.name = params.name;
            this.size = parseInt(params.size);
            this.time = parseInt(params.time);
        }
        init() {
            document.getElementById('name').innerText = this.name;
            document.getElementById('size').innerText = this.size;
            document.getElementById('time').innerText = this.time;
        }
    }

    // TODO Step 6 implement getTemplate() {}

    // put component in global scope, tu be runnable right from the HTML.
    // TODO Step 6 export ScoreComponent
    window.ScoreComponent = ScoreComponent;
})();
